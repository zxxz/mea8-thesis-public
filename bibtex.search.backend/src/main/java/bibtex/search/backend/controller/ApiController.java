package bibtex.search.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@Slf4j
public class ApiController {

    @Autowired
    private SearchService searchService;

    @GetMapping("/overview-unfiltered")
    public void getOverviewUnfiltered(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "overview-unfiltered.csv" + "\"");

        String csvContent = searchService.getOverviewUnfilteredCSV();

        response.getWriter().write(csvContent);
    }

    @GetMapping("/filtered-by-language")
    public void getFilterByLanguage(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "filtered-by-language.csv" + "\"");

        String csvContent = searchService.getFilteredByLanguageCSV();

        response.getWriter().write(csvContent);
    }

    @GetMapping("/filtered-by-language-and-published")
    public void getFilteredByLanguageAndPublished(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "filtered-by-language-and-published.csv" + "\"");

        String csvContent = searchService.getFilteredByLanguageAndPublishedCSV();

        response.getWriter().write(csvContent);
    }

    @GetMapping("/filtered-by-language-and-published-and-keywords")
    public void getFilteredByLanguageAndPublishedAndKeywords(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "filtered-by-language-and-published-and-keywords.csv" + "\"");

        String csvContent = searchService.getFilteredByLanguageAndPublishedAndKeywordsCSV();

        response.getWriter().write(csvContent);
    }

    @GetMapping("/filtered-by-language-and-published-and-keywords-no-duplicates")
    public void getFilteredByLanguageAndPublishedAndKeywordsNoDuplicates(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "filtered-by-language-and-published-and-keywords-no-duplicates.csv" + "\"");

        String csvContent = searchService.getFilteredByLanguageAndPublishedAndKeywordsNoDuplicatesCSV();

        response.getWriter().write(csvContent);
    }
}
