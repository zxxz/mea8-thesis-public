package bibtex.search.backend.controller;

import bibtex.search.backend.solr.BibTeXDocument;
import bibtex.search.backend.solr.BibTeXDocumentIndexer;
import bibtex.search.backend.solr.BibTeXDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static bibtex.search.backend.solr.BibTeXDocumentIndexer.DATABASES;
import static bibtex.search.backend.solr.BibTeXDocumentIndexer.QUERIES;

@Component
public class SearchService {

    @Autowired
    private BibTeXDocumentRepository repository;

    public String getOverviewUnfilteredCSV() {
        FacetPage<BibTeXDocument> results = repository.getOverviewUnfiltered(PageRequest.of(0, Integer.MAX_VALUE));

        Map<String, Long> map = new HashMap<>();

        List<FacetFieldEntry> facets = results.getFacetResultPage("source").getContent();
        facets.forEach(entry -> {
            map.put(entry.getValue(), entry.getValueCount());
        });

        return getCSV("", map);
    }

    public String getFilteredByLanguageCSV() {
        FacetPage<BibTeXDocument> results = repository.getFilteredByLanguage(PageRequest.of(0, Integer.MAX_VALUE));

        Map<String, Long> map = new HashMap<>();

        results.getFacetQueryResult().getContent().forEach(facetQueryEntry -> {
            map.put(facetQueryEntry.getValue(), facetQueryEntry.getValueCount());
        });

        return getCSV("source:", map);
    }

    public String getFilteredByLanguageAndPublishedCSV() {
        FacetPage<BibTeXDocument> results = repository.getFilteredByLanguageAndPublished(PageRequest.of(0, Integer.MAX_VALUE));

        Map<String, Long> map = new HashMap<>();

        results.getFacetQueryResult().getContent().forEach(facetQueryEntry -> {
            map.put(facetQueryEntry.getValue(), facetQueryEntry.getValueCount());
        });

        return getCSV("source:", map);
    }


    public String getFilteredByLanguageAndPublishedAndKeywordsCSV() {
        FacetPage<BibTeXDocument> results = repository.getFilteredByLanguageAndPublishedAndKeywords(PageRequest.of(0, Integer.MAX_VALUE));

        Map<String, Long> map = new HashMap<>();

        results.getFacetQueryResult().getContent().forEach(facetQueryEntry -> {
            map.put(facetQueryEntry.getValue(), facetQueryEntry.getValueCount());
        });

        return getCSV("source:", map);
    }


    public String getFilteredByLanguageAndPublishedAndKeywordsNoDuplicatesCSV() {
        FacetPage<BibTeXDocument> results = repository.getFilteredByLanguageAndPublishedAndKeywords(PageRequest.of(0, Integer.MAX_VALUE));

        Map<String, BibTeXDocument> map = new HashMap<>();

        results.getContent().forEach(bibTeXDocument -> {
            map.put(bibTeXDocument.getKey(), bibTeXDocument);
        });

        StringBuffer output = new StringBuffer();

        output.append("#;#;Key;Title;Abstract;Year");
        output.append("\n");

        int id = 1;
        for (BibTeXDocument document : map.values()) {
            output.append(id + ";" + "S" + id + ";" + document.getKey() + ";" + document.getTitle() + ";" + "\"" + document.getAbstractText() + "\"" + ";" + document.getYear());
            output.append("\n");
            id++;
        }

        return output.toString();
    }

    private String getCSV(String prefix, Map<String, Long> map) {
        StringBuffer output = new StringBuffer();

        output.append("Query;#;Database;Count");
        output.append("\n");
        int grandTotal = 0;
        for (String query : QUERIES) {
            int total = 0;
            for (String database : DATABASES) {
                Long count = map.get(prefix + query + database);
                total += count;
                output.append(query + ";" + database + ";" + BibTeXDocumentIndexer.DATABASE_NAMES.get(database) + ";" + count);
                output.append("\n");
            }
            output.append("" + ";" + "" + ";" + "Total" + ";" + total);
            output.append("\n");

            grandTotal += total;
        }
        output.append("" + ";" + "" + ";" + "Grand Total" + ";" + grandTotal);
        output.append("\n");
        return output.toString();
    }
}
