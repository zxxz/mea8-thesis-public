package bibtex.search.backend.bibtex;

import bibtex.search.backend.solr.BibTeXDocument;
import com.github.pemistahl.lingua.api.Language;
import com.github.pemistahl.lingua.api.LanguageDetector;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class BibTeXDocumentBuilder {

    public static final Set<String> SUPPORTED_FIELDS = new HashSet<>(Arrays.asList(
            "abstract", "author", "doi", "issn", "ISSN", "journal", "number", "pages", "title", "volume", "year",
            "isbn", "publisher", "keywords", "url", "URL", "type",
            "month", "annote", "address", "archivePrefix", "arxivId", "eprint", "pmid",
            "booktitle", "bookTitle", "organization", "school",
            "institution", "editor", "series",
            "numpages", "acmid", "issue_date", "location", "articleno", "note", "day"
    ));

    public static final Set<String> IGNORED_FIELDS = new HashSet<>(Arrays.asList("__markedentry"));

    @Autowired
    private LanguageDetector detector;

    public BibTeXDocument buildBibTeXDocument(BibTeXEntry entry) {
        return BibTeXDocument.builder()
//                .id(UUID.randomUUID().toString())
//                .id(entry.getKey().getValue())
                .key(entry.getKey().getValue())
                .type(getEntryType(entry))
                .abstractText(sanitize(getValue(entry, "abstract")))
                .language(detectAbstractLanguage(getValue(entry, "abstract"), getValue(entry, BibTeXEntry.KEY_TITLE)))
                .author(getValue(entry, BibTeXEntry.KEY_AUTHOR))
                .doi(getValue(entry, BibTeXEntry.KEY_DOI))
                .issn(getValue(entry, "issn", "ISSN"))
                .journal(getValue(entry, BibTeXEntry.KEY_JOURNAL))
                .number(getValue(entry, BibTeXEntry.KEY_NUMBER))
                .pages(getValue(entry, BibTeXEntry.KEY_PAGES))
                .title(sanitize(getValue(entry, BibTeXEntry.KEY_TITLE)))
                .volume(getValue(entry, BibTeXEntry.KEY_VOLUME))
                .year(getValue(entry, BibTeXEntry.KEY_YEAR))
                .isbn(getValue(entry, "isbn"))
                .publisher(getValue(entry, BibTeXEntry.KEY_PUBLISHER))
                .keywords(getValue(entry, "keywords"))
                .url(getValue(entry, "url", "URL"))
                .month(getValue(entry, BibTeXEntry.KEY_MONTH))
                .annote(getValue(entry, BibTeXEntry.KEY_ANNOTE))
                .address(getValue(entry, BibTeXEntry.KEY_ADDRESS))
                .archivePrefix(getValue(entry, "archivePrefix"))
                .arxivId(getValue(entry, "arxivId"))
                .eprint(getValue(entry, "eprint"))
                .pmid(getValue(entry, "pmid"))
                .bookTitle(getValue(entry, "booktitle"))
                .organization(getValue(entry, "organization"))
                .school(getValue(entry, "school"))
                .institution(getValue(entry, "institution"))
                .editor(getValue(entry, "editor"))
                .series(getValue(entry, "series"))
                .numPages(getValue(entry, "numpages"))
                .acmid(getValue(entry, "acmid"))
                .issueDate(getValue(entry, "issue_date"))
                .location(getValue(entry, "location"))
                .articleNumber(getValue(entry, "articleno"))
                .note(getValue(entry, "note"))
                .day(getValue(entry, "day"))

                .build();
    }

    private String sanitize(String input) {
        if (input != null) {
            String noCopyright = input.replaceAll("\\\\textcopyright", "");
            String noNewLines = noCopyright.replaceAll("\n", " ");
            return removeCurlyBraces(noNewLines);
        }
        return "";
    }

    private String removeCurlyBraces(String value) {
        if (value == null) {
            return null;
        }
        value = value.replaceAll("\\{", "");
        value = value.replaceAll("}", "");
        return value;
    }

    private Language detectAbstractLanguage(String abstractText, String title) {
        if (abstractText != null) {
            return detector.detectLanguageOf(abstractText);
        }
        return detector.detectLanguageOf(title);
    }

    private String getEntryType(BibTeXEntry entry) {
        String type1 = entry.getType().getValue();
        String type2 = getValue(entry, "type");

//        if (type1.equals(type2)) {
//            log.info("Found another type field: " + type1);
//        } else if (type2 != null) {
//            log.warn("Found 2 types: [type1 = " + type1 + "]" + "[type2 = " + type2 + "]");
//        }
        return type1;
    }

    private String getValue(BibTeXEntry entry, Key key) {
        org.jbibtex.Value field = entry.getField(key);
        if (field != null) {
            return field.toUserString();
        }
        return null;
    }

    private String getValue(BibTeXEntry entry, String key) {
        return getValue(entry, key, null);
    }

    private String getValue(BibTeXEntry entry, String key, String alternativeKey) {
        String value = getValue(entry, new Key(key));
        if (value == null && alternativeKey != null) {
            return getValue(entry, new Key(alternativeKey));
        }
        return value;
    }
}
