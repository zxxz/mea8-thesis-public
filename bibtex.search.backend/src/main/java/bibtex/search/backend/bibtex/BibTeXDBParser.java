package bibtex.search.backend.bibtex;

import lombok.extern.slf4j.Slf4j;
import org.jbibtex.*;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class BibTeXDBParser {

    public Map<Key, BibTeXEntry> parseBibTeXDB(String filename) {
        try {
            BibTeXParser bibTeXParser = new BibTeXParser();
            BibTeXDatabase bibTeXDatabase = bibTeXParser.parseFully(new FileReader(filename));

            List<Exception> exceptions = bibTeXParser.getExceptions();
            exceptions.forEach(e -> {
                log.error(e.getMessage());
            });

            Map<Key, BibTeXEntry> entries = bibTeXDatabase.getEntries();

            log.info("Library: " + filename + " " + "Parsed " + entries.size() + " BibTeX entries");

            return entries;
        } catch (ParseException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return Collections.emptyMap();
    }
}
