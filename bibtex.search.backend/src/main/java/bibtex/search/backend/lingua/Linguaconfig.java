package bibtex.search.backend.lingua;

import com.github.pemistahl.lingua.api.LanguageDetector;
import com.github.pemistahl.lingua.api.LanguageDetectorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Linguaconfig {

    @Bean
    public LanguageDetector detector() {
        return LanguageDetectorBuilder.fromAllBuiltInLanguages().build();
    }
}
