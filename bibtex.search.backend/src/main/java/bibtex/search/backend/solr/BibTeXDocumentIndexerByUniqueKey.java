package bibtex.search.backend.solr;

import bibtex.search.backend.bibtex.BibTeXDBParser;
import bibtex.search.backend.bibtex.BibTeXDocumentBuilder;
import lombok.extern.slf4j.Slf4j;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static bibtex.search.backend.solr.BibTeXDocumentIndexer.DATABASES;
import static bibtex.search.backend.solr.BibTeXDocumentIndexer.QUERIES;

@Component
@Slf4j
public class BibTeXDocumentIndexerByUniqueKey {

    @Autowired
    private BibTeXDBParser bibTeXDBParser;

    @Autowired
    private BibTeXDocumentBuilder bibTeXDocumentBuilder;

    private Map<String, BibTeXDocument> documentMap = new HashMap<>();

    public Collection<BibTeXDocument> index(String bibTeXLibrary) {

        documentMap.clear();

        for (String query : QUERIES) {
            for (String database : DATABASES) {
                Map<Key, BibTeXEntry> bibTeXDB = bibTeXDBParser.parseBibTeXDB(bibTeXLibrary + "/" + query + "/" + query + "-" + database + ".bib");
                populateDocumentMap(query, database, bibTeXDB);
            }
        }

        return documentMap.values();
    }

    private void populateDocumentMap(String query, String database, Map<Key, BibTeXEntry> bibTeXDB) {

        bibTeXDB.forEach((key, entry) -> {

//            detectFieldsNotYetIndexed(entry);

            BibTeXDocument document = bibTeXDocumentBuilder.buildBibTeXDocument(entry);
            document.setId(entry.getKey().getValue());
            document.setRaw("false");
            if (documentMap.containsKey(document.getId())) {
                log.info("FOUND A DUPLICATE DOCUMENT " + document.getId());
            } else {
                documentMap.put(document.getKey(), document);
            }
        });
    }

    private void detectFieldsNotYetIndexed(BibTeXEntry entry) {
        Map<Key, org.jbibtex.Value> fields = entry.getFields();
        Set<Key> keys = fields.keySet();
        keys.forEach(key -> {
            if (!BibTeXDocumentBuilder.SUPPORTED_FIELDS.contains(key.getValue()) && !BibTeXDocumentBuilder.IGNORED_FIELDS.contains(key.getValue())) {
                log.info("KEY MISSING:" + key);
            }
        });
    }

}
