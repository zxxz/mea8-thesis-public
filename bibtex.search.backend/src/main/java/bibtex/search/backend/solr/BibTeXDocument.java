package bibtex.search.backend.solr;

import com.github.pemistahl.lingua.api.Language;
import lombok.Builder;
import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(collection = "bibtex")
@Data
@Builder
public class BibTeXDocument {

    @Id
    private String id;

    @Field
    private String type;

    @Field
    private String key;

    @Field
    private String abstractText;

    @Field
    private Language language;

    @Field
    private String author;

    @Field
    private String doi;

    @Field
    private String issn;

    @Field
    private String journal;

    @Field
    private String number;

    @Field
    private String pages;

    @Field
    private String title;

    @Field
    private String volume;

    @Field
    private String year;

    @Field
    private String isbn;

    @Field
    private String publisher;

    @Field
    private String keywords;

    @Field
    private String url;

    @Field
    private String month;

    @Field
    private String annote;

    @Field
    private String address;

    @Field
    private String archivePrefix;

    @Field
    private String arxivId;

    @Field
    private String eprint;

    @Field
    private String pmid;

    @Field
    private String bookTitle;

    @Field
    private String organization;

    @Field
    private String school;

    @Field
    private String institution;

    @Field
    private String editor;

    @Field
    private String series;

    @Field
    private String numPages;

    @Field
    private String acmid;

    @Field
    private String issueDate;

    @Field
    private String location;

    @Field
    private String articleNumber;

    @Field
    private String note;

    @Field
    private String day;

    @Field
    private String query;

    @Field
    private String database;

    @Field
    private String source;

    @Field
    private String raw;
}
