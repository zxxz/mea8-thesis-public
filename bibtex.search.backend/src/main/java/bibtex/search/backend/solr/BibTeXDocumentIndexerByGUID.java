package bibtex.search.backend.solr;

import bibtex.search.backend.bibtex.BibTeXDBParser;
import bibtex.search.backend.bibtex.BibTeXDocumentBuilder;
import lombok.extern.slf4j.Slf4j;
import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static bibtex.search.backend.solr.BibTeXDocumentIndexer.DATABASES;
import static bibtex.search.backend.solr.BibTeXDocumentIndexer.QUERIES;

@Component
@Slf4j
public class BibTeXDocumentIndexerByGUID {

    @Autowired
    private BibTeXDBParser bibTeXDBParser;

    @Autowired
    private BibTeXDocumentBuilder bibTeXDocumentBuilder;

    public List<BibTeXDocument> index(String bibTeXLibrary) {

        List<BibTeXDocument> documents = new ArrayList<>();

        for (String query : QUERIES) {
            for (String database : DATABASES) {
                Map<Key, BibTeXEntry> bibTeXDB = bibTeXDBParser.parseBibTeXDB(bibTeXLibrary + "/" + query + "/" + query + "-" + database + ".bib");
                documents.addAll(buildDocuments(query, database, bibTeXDB));
            }
        }

        return documents;
    }

    private List<BibTeXDocument> buildDocuments(String query, String database, Map<Key, BibTeXEntry> bibTeXDB) {

        List<BibTeXDocument> documents = new ArrayList<>();

        bibTeXDB.forEach((key, entry) -> {

//            detectFieldsNotYetIndexed(entry);

            BibTeXDocument document = bibTeXDocumentBuilder.buildBibTeXDocument(entry);
            document.setId(UUID.randomUUID().toString());
            document.setQuery(query);
            document.setDatabase(database);
            document.setSource(query + database);
            document.setRaw("true");
            documents.add(document);
        });

        return documents;
    }
}
