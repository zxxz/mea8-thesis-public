package bibtex.search.backend.solr;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.repository.Facet;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface BibTeXDocumentRepository extends SolrCrudRepository<BibTeXDocument, String> {

    @Query(value = "*:*", filters = {"journal:[* TO *]", "language:ENGLISH"})
    @Facet(fields = {"journal"}, limit = -1)
    FacetPage<BibTeXDocument> findAllByJournalNotNullAndLanguageEnglish(Pageable page);

    @Query(value = "*:*", filters = {"raw:true"})
    @Facet(fields = {"source"}, limit = -1)
    FacetPage<BibTeXDocument> getOverviewUnfiltered(Pageable pageable);

    @Query(value = "*:*", filters = {"raw:true", "language:ENGLISH"})
    @Facet(queries = {
            "source:Q1DB1", "source:Q1DB2", "source:Q1DB3", "source:Q1DB4", "source:Q1DB5", "source:Q1DB6",
            "source:Q2DB1", "source:Q2DB2", "source:Q2DB3", "source:Q2DB4", "source:Q2DB5", "source:Q2DB6"
    }, limit = -1)
    FacetPage<BibTeXDocument> getFilteredByLanguage(PageRequest page);

    @Query(value = "*:*", filters = {"raw:true", "language:ENGLISH", "journal:[* TO *]"})
    @Facet(queries = {
            "source:Q1DB1", "source:Q1DB2", "source:Q1DB3", "source:Q1DB4", "source:Q1DB5", "source:Q1DB6",
            "source:Q2DB1", "source:Q2DB2", "source:Q2DB3", "source:Q2DB4", "source:Q2DB5", "source:Q2DB6"
    }, limit = -1)
    FacetPage<BibTeXDocument> getFilteredByLanguageAndPublished(PageRequest of);

    @Query(value = "" +
            "abstractText:(" +
            "\"Enterprise Architecture Implementation\" OR " +
            "\"Enterprise Architecture Implementation Methodology\" OR " +
            "\"Enterprise Architecture Implementation Practice\" OR " +
            "\"Enterprise Architecture Implementation Method\" OR " +
            "\"Enterprise Architecture Development\" OR " +
            "\"Enterprise Architecture Implementation Process\" OR " +
            "\"Enterprise Architecture Implementation Planning\" OR " +
            "\"Enterprise Architecture Transition Plan\" OR " +
            "\"Enterprise Architecture Transformation Process\" OR " +
            "\"Enterprise Architecture Implementation Requirement\"" +
            ") OR " +
            "abstractText:(" +
            "\"Enterprise Architecture Development and Implementation\" OR " +
            "\"Enterprise Architecture Lifecycle Phases\" OR " +
            "\"Enterprise Architecture Governance\" OR " +
            "\"Enterprise Architecture Competency Development\"" +
            ") OR " +
            "title:(" +
            "\"Enterprise Architecture Implementation\" OR " +
            "\"Enterprise Architecture Implementation Methodology\" OR " +
            "\"Enterprise Architecture Implementation Practice\" OR " +
            "\"Enterprise Architecture Implementation Method\" OR " +
            "\"Enterprise Architecture Development\" OR " +
            "\"Enterprise Architecture Implementation Process\" OR " +
            "\"Enterprise Architecture Implementation Planning\" OR " +
            "\"Enterprise Architecture Transition Plan\" OR " +
            "\"Enterprise Architecture Transformation Process\" OR " +
            "\"Enterprise Architecture Implementation Requirement\"" +
            ") OR " +
            "title:(" +
            "\"Enterprise Architecture Development and Implementation\" OR " +
            "\"Enterprise Architecture Lifecycle Phases\" OR " +
            "\"Enterprise Architecture Governance\" OR " +
            "\"Enterprise Architecture Competency Development\"" +
            ")", filters = {"raw:true", "language:ENGLISH", "journal:[* TO *]"})
    @Facet(queries = {
            "source:Q1DB1", "source:Q1DB2", "source:Q1DB3", "source:Q1DB4", "source:Q1DB5", "source:Q1DB6",
            "source:Q2DB1", "source:Q2DB2", "source:Q2DB3", "source:Q2DB4", "source:Q2DB5", "source:Q2DB6"
    }, limit = -1)
    FacetPage<BibTeXDocument> getFilteredByLanguageAndPublishedAndKeywords(PageRequest of);
}
