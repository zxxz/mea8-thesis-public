package bibtex.search.backend.solr;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class BibTeXDocumentIndexer {

    @Autowired
    private BibTeXDocumentRepository repository;

    @Value("${bibtex.library}")
    private String bibTeXLibrary;

    public static final String[] QUERIES = new String[]{"Q1", "Q2"};
    public static final String[] DATABASES = new String[]{"DB1", "DB2", "DB3", "DB4", "DB5", "DB6"};
    public static final Map<String, String> DATABASE_NAMES = createDatabaseMap();

    private static Map<String, String> createDatabaseMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("DB1", "ACM Digital Library");
        map.put("DB2", "IEEE Xplore");
        map.put("DB3", "Science Direct - Elsevier");
        map.put("DB4", "Springer Link");
        map.put("DB5", "Taylor and Francis");
        map.put("DB6", "Google Scholar");
        return map;
    }

    @Autowired
    private BibTeXDocumentIndexerByUniqueKey indexerByUniqueKey;

    @Autowired
    private BibTeXDocumentIndexerByGUID indexerByGUID;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {

        repository.deleteAll();

        Collection<BibTeXDocument> documentsWithGUID = indexerByGUID.index(bibTeXLibrary);
        log.info("Parsed a total of: " + documentsWithGUID.size() + " documents with GUID");
        repository.saveAll(documentsWithGUID);

        Collection<BibTeXDocument> documentsWithUniqueKey = indexerByUniqueKey.index(bibTeXLibrary);
        log.info("Parsed a total of: " + documentsWithUniqueKey.size() + " documents with unique key");
        repository.saveAll(documentsWithUniqueKey);
    }
}
