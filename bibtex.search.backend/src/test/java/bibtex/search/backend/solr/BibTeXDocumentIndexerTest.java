package bibtex.search.backend.solr;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BibTeXDocumentIndexerTest {

    @Value("${bibtex.library}")
    private String bibTeXLibrary;

    @Test
    public void readAllLibraryFilesPerDbAndPerQuery() {

        try {

            Stream<Path> q1Files = Files.list(Paths.get(bibTeXLibrary + File.separator + "Q1"))
                    .filter(path -> path.toString().endsWith(".bib"));

            Stream<Path> q2Files = Files.list(Paths.get(bibTeXLibrary + File.separator + "Q2"))
                    .filter(path -> path.toString().endsWith(".bib"));

            q1Files.forEach(path -> {
                log.info(path.toString());
            });

            q2Files.forEach(path -> {
                log.info(path.toString());
            });


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
