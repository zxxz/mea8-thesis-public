# mea8-thesis-public

## Introduction
This repository exists to support the Systematic Literature Review of my Master Thesis for the **MEA8 Master in Enterprise Architecture** at [IC Institute, an INNOCOM Company](https://www.icinstitute.be). 

All *inclusion* and *exclusion* criteria are then executed against the Search Server.

## Installation and Setup

Download or clone this repository https://gitlab.com/zxxz/mea8-thesis-public.

### Solr
Unzip Apache [Solr v8.1.1](solr-8.1.1.zip).

Define:
- `${PORT}` (for example `8983`)
- `${SOLR_HOME}` [`bibtex.search.solr/solr-home`](bibtex.search.solr/solr-home)
- `${SEARCH_INDEX}` where the search index is stored (for example `/tmp`)

Start Solr with the following command:
```bash
bin/solr start -p ${PORT} -s ${SOLR_HOME} -t ${SEARCH_INDEX}
```

### Java Backend

Define:
- `${BIBEX_LIBRARY}` [`search-results/step-02/mea8-thesis-library.bib`](search-results/step-02/mea8-thesis-library.bib)
- `${SOLR_SERVER}` (for example `http://localhost:18983/solr`)

Build and launch the backend:
```bash
cd bibtex.search.backend
mvn clean install -DskipTests
java -jar target/bibtex.search.backend-*.jar \
--bibtex.library=${BIBEX_LIBRARY} \
--spring.data.solr.host=${SOLR_SERVER}
```

At startup the java backend parses and indexes the configured BibTeX Library.

### Start searching

It's possible to interact with the search server in many ways, for example:
- Via the Solr interface: http://localhost:18983/solr/#/
- Via any HTTP Client (for example `curl` or [Postman](https://www.getpostman.com) and [`http://localhost:18983/solr/bibtex/select?q=*:*`](http://localhost:18983/solr/bibtex/select?q=*:*))

## Analysis

### 1. Overview of unfiltered list of initial candidate studies after import in search server

Request (JSON)
```bash
curl -X GET \ 
    'http://localhost:18983/solr/bibtex/select?q=*:*&rows=0&start=0&facet=on&fq=raw:true&facet.field=source' 
```

Request (CSV)
```bash
curl -X GET \ 
    'http://localhost:8080/overview-unfiltered'
```

### 2. Include only studies written in English

Request (JSON)
```bash
curl -X GET \ 
    'http://localhost:18983/solr/bibtex/select?q=*:*&rows=0&start=0&facet=on&fq=raw:true&fq=language:ENGLISH&facet.query=source:Q1DB1&facet.query=source:Q1DB2&facet.query=source:Q1DB3&facet.query=source:Q1DB4&facet.query=source:Q1DB5&facet.query=source:Q1DB6&facet.query=source:Q2DB1&facet.query=source:Q2DB2&facet.query=source:Q2DB3&facet.query=source:Q2DB4&facet.query=source:Q2DB5&facet.query=source:Q2DB6'
```

Request (CSV)
```bash
curl -X GET \
    'http://localhost:8080/filtered-by-language'
```

### 3. Include only published studies

Request (JSON)
```bash
curl -X GET \ 
    'http://localhost:18983/solr/bibtex/select?q=*:*&rows=0&start=0&facet=on&fq=raw:true&fq=journal:[* TO *]&fq=language:ENGLISH&facet.query=source:Q1DB1&facet.query=source:Q1DB2&facet.query=source:Q1DB3&facet.query=source:Q1DB4&facet.query=source:Q1DB5&facet.query=source:Q1DB6&facet.query=source:Q2DB1&facet.query=source:Q2DB2&facet.query=source:Q2DB3&facet.query=source:Q2DB4&facet.query=source:Q2DB5&facet.query=source:Q2DB6'
```

Request (CSV)
```bash
curl -X GET \ 
    'http://localhost:8080/filtered-by-language-and-published'
```

### 4. Include studies with exact keywords match

Request (JSON)
```bash
curl -X GET \ 
    'http://localhost:18983/solr/bibtex/select?q=abstractText:("Enterprise Architecture Implementation" OR "Enterprise Architecture Implementation Methodology" OR "Enterprise Architecture Implementation Practice" OR "Enterprise Architecture Implementation Method" OR "Enterprise Architecture Development" OR "Enterprise Architecture Implementation Process" OR "Enterprise Architecture Implementation Planning" OR "Enterprise Architecture Transition Plan" OR "Enterprise Architecture Transformation Process" OR "Enterprise Architecture Implementation Requirement") OR abstractText:("Enterprise Architecture Development and Implementation" OR "Enterprise Architecture Lifecycle Phases" OR "Enterprise Architecture Governance" OR "Enterprise Architecture Competency Development") OR title:("Enterprise Architecture Implementation" OR "Enterprise Architecture Implementation Methodology" OR "Enterprise Architecture Implementation Practice" OR "Enterprise Architecture Implementation Method" OR "Enterprise Architecture Development" OR "Enterprise Architecture Implementation Process" OR "Enterprise Architecture Implementation Planning" OR "Enterprise Architecture Transition Plan" OR "Enterprise Architecture Transformation Process" OR "Enterprise Architecture Implementation Requirement") OR title:("Enterprise Architecture Development and Implementation" OR "Enterprise Architecture Lifecycle Phases" OR "Enterprise Architecture Governance" OR "Enterprise Architecture Competency Development")&rows=0&start=0&facet=on&fq=raw:true&fq=journal:[* TO *]&fq=language:ENGLISH&facet.query=source:Q1DB1&facet.query=source:Q1DB2&facet.query=source:Q1DB3&facet.query=source:Q1DB4&facet.query=source:Q1DB5&facet.query=source:Q1DB6&facet.query=source:Q2DB1&facet.query=source:Q2DB2&facet.query=source:Q2DB3&facet.query=source:Q2DB4&facet.query=source:Q2DB5&facet.query=source:Q2DB6'
```

Request (CSV)
```bash
curl -X GET \
    'http://localhost:8080/filtered-by-language-and-published-and-keywords'
```
## Credits

The open source [JBibTeX](https://github.com/jbibtex/jbibtex) library is used for parsing `.bib` files. 

The open source [lingua](https://github.com/pemistahl/lingua) library is used to detect the language of each abstract or title.

## License

All custom code in this repository is licensed under [BSD 3-Clause License](https://opensource.org/licenses/BSD-3-Clause).
