#!/usr/bin/env bash

PORT=18983
SOLR_HOME=bibtex.search.solr/solr-home
SEARCH_INDEX=/tmp

solr-8.1.1/bin/solr restart -p ${PORT} -s ${SOLR_HOME} -t ${SEARCH_INDEX}
