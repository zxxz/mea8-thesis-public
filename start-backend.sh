#!/usr/bin/env bash

BIBEX_LIBRARY=${PWD}/search-results/step-1
SOLR_SERVER=http://localhost:18983/solr

cd bibtex.search.backend
mvn clean install -DskipTests
java -jar target/bibtex.search.backend-*.jar \
--bibtex.library=${BIBEX_LIBRARY} \
--spring.data.solr.host=${SOLR_SERVER}
